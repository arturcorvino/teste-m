<?php
namespace App\Test\TestCase\Controller\Component;

use App\Controller\Component\SentEmailComponent;
use Cake\Controller\ComponentRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Controller\Component\SentEmailComponent Test Case
 */
class SentEmailComponentTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Controller\Component\SentEmailComponent
     */
    public $SentEmail;

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $registry = new ComponentRegistry();
        $this->SentEmail = new SentEmailComponent($registry);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SentEmail);

        parent::tearDown();
    }

    /**
     * Test initial setup
     *
     * @return void
     */
    public function testInitialization()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
