<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\Controller\ComponentRegistry;
use Cake\Mailer\Email;

/**
 * SentEmail component
 */
class EmailComponent extends Component
{

    public $emailFron = ['artur.corvino1@gmail.com' => 'Artur F. C. Rosa'];

    /**
     * Default configuration.
     *
     * @var array
     */
    protected $_defaultConfig = [];

    public function sent($data) {

        $message = null;

        if (($data['html'] >= 7 && $data['html'] <= 10) || ($data['css'] >= 7 && $data['css'] <= 10) || ($data['javascript'] >= 7 && $data['javascript'] <= 10)) {
            $message[] = $this->__frontEnd();
        }

        if (($data['android'] >= 7 && $data['android'] <= 10) || ($data['ios'] >= 7 && $data['ios'] <= 10)) {
            $message[] = $this->__mobile();
        }

        if (($data['python'] >= 7 && $data['python'] <= 10) || ($data['django'] >= 7 && $data['django'] <= 10)) {
            $message[] = $this->__backEnd();
        }

        if (!$message) {
            $message[] = $this->__comum();
        }

        $this->__sent($message, $data);
    }

    private function __frontEnd() {
        return [
            'subject' => 'Obrigado por se candidatar a vaga de Front-End',
            'message' => 'Obrigado por se candidatar, assim que tivermos uma vaga disponível para programador Front-End entraremos em contato.'
        ];
    }

    private function __backEnd() {
        return [
            'subject' => 'Obrigado por se candidatar a vaga de Back-End',
            'message' => 'Obrigado por se candidatar, assim que tivermos uma vaga disponível para programador Back-End entraremos em contato.'
        ];
    }

    private function __mobile() {
        return [
            'subject' => 'Obrigado por se candidatar a vaga de Mobile',
            'message' => 'Obrigado por se candidatar, assim que tivermos uma vaga disponível para programador Mobile entraremos em contato.'
        ];
    }

    private function __comum() {
        return [
            'subject' => 'Obrigado por se candidatar',
            'message' => 'Obrigado por se candidatar, assim que tivermos uma vaga disponível para programador entraremos em contato.'
        ];
    }

    private function __sent($messages, $data) {
        foreach ($messages as $message) {
            $message['message'] = "Olá {$data['name']}! ".$message['message'];

            $email = new Email('default');
            $email->from($this->emailFron);
            $email->to($data['email']);
            $email->subject($message['subject']);
            $email->send($message['message']);
        }
    }

}
