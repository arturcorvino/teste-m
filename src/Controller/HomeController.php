<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Mailer\Email;

/**
 * Home Controller
 *
 * @property \App\Model\Table\HomeTable $Home
 */
class HomeController extends AppController
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();

        $this->loadComponent('Email');
    }

    /**
     * Index method
     *
     * @return \Cake\Network\Response|null
     */
    public function index()
    {
        if ($this->request->is(['post', 'put'])) {
            $request = $this->request->data;

            if (empty($request['name']) || empty($request['email'])) {
                return $this->Flash->error('Atenção, um ou mais campos não foram preenchidos. Por favor, verifique e preencha todos os campos obrigatórios.');
            }

            $this->Email->sent($request);

            $this->request->data = [];
            return $this->Flash->success("Obrigado <b>{$request['name']}</b> pelas informações. assim que tivermos uma vaga, entraremos em contato.");
        }
    }

}
