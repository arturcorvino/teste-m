<?php $this->assign('title', "Concorrer a vaga"); ?>
<?php $options = [
    null => '',
    0 => 0,
    1 => 1,
    2 => 2,
    3 => 3,
    4 => 4,
    5 => 5,
    6 => 6,
    7 => 7,
    8 => 8,
    9 => 9,
    10 => 10,
]; ?>

<div class="row">
    <div class="col-lg-6 col-lg-push-3 col-sm-10 col-sm-push-1">
        <section class="content-inner margin-top-no">
            <div class="card">
                <div class="card-main">
                    <?= $this->Form->create('Developer', []); ?>
                        <div class="card-inner">
                            <div class="row">
                                <div class="col-md-12">
                                    <?= $this->Flash->render() ?>
                                    <p>Olá, estamos buscando desenvolvedores acima da média, para conseguirmos montar uma equipe fodastica e fazer as coisas mais incríveis possível. Caso você se considere um desenvolvedor incrível, informe seus dados abaixo, assim que possível entraremos em contato. Boa sorte :) </p>
                                    <p style="color: red;">Os campos marcados com * são de preenchimento obrigatório.</p>
                                    <div class="form-group form-group-label">
                                        <label for="ui_floating_label_example" class="floating-label"><i style="color: red;">*</i> Nome</label>
                                        <?= $this->Form->input('name',
                                            [
                                                'class' => 'form-control',
                                                'label' => false,
                                            ]
                                        ); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group form-group-label">
                                        <label for="ui_floating_label_example" class="floating-label"><i style="color: red;">*</i> Email</label>
                                        <?= $this->Form->input('email',
                                            [
                                                'class' => 'form-control',
                                                'label' => false,
                                            ]
                                        ); ?>
                                    </div>
                                    <p>Para conhecermos melhor um pouco do seu perfil, informe o seu conhecimento em cada uma das tecnologias abaixo, onde 0 é igual a "Nenhum conhecimento" e 10 é igual a "Expert na tecnologia." </p>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-label">
                                        <label for="ui_floating_label_example" class="floating-label">Html</label>
                                        <?= $this->Form->input('html',
                                            [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'options' => $options
                                            ]
                                        ); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-label">
                                        <label for="ui_floating_label_example" class="floating-label">CSS</label>
                                        <?= $this->Form->input('css',
                                            [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'options' => $options
                                            ]
                                        ); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-label">
                                        <label for="ui_floating_label_example" class="floating-label">JavaScript</label>
                                        <?= $this->Form->input('javascript',
                                            [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'options' => $options
                                            ]
                                        ); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-label">
                                        <label for="ui_floating_label_example" class="floating-label">Python</label>
                                        <?= $this->Form->input('python',
                                            [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'options' => $options
                                            ]
                                        ); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-label">
                                        <label for="ui_floating_label_example" class="floating-label">Django</label>
                                        <?= $this->Form->input('django',
                                            [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'options' => $options
                                            ]
                                        ); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-label">
                                        <label for="ui_floating_label_example" class="floating-label">Desenvolvedor IOS</label>
                                        <?= $this->Form->input('ios',
                                            [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'options' => $options
                                            ]
                                        ); ?>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group form-group-label">
                                        <label for="ui_floating_label_example" class="floating-label">Desenvolvedor Android</label>
                                        <?= $this->Form->input('android',
                                            [
                                                'class' => 'form-control',
                                                'label' => false,
                                                'options' => $options
                                            ]
                                        ); ?>
                                    </div>
                                </div>
                                <div class="col-md-12">

                                </div>
                            </div>
                        </div>
                        <div class="card-action">
                            <div class="card-action-btn pull-left">
                                <?= $this->Form->button('Enviar dados', ['class' => 'btn btn-flat btn-brand-accent waves-attach waves-effect']); ?>
                            </div>
                        </div>
                    <?= $this->Form->end(); ?>
                </div>
            </div>
        </section>
    </div>
</div>
