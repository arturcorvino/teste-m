<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="initial-scale=1.0, maximum-scale=1.0, user-scalable=no, width=device-width" name="viewport">
    <title>Meus Pedidos - <?= $this->fetch('title') ?></title>

    <?= $this->Html->meta('icon') ?>

    <!-- css -->
    <?= $this->Html->css([
        'base',
        'project',
    ]) ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>

</head>
<body class="page-brand">

    <?= $this->element('header'); ?>

    <main class="content">
        <div class="content-header ui-content-header">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-lg-push-3 col-sm-10 col-sm-push-1">
                        <h1 class="content-heading"><?= $this->fetch('title'); ?></h1>
                    </div>
                </div>
            </div>
        </div>
        <div class="container">
            <?= $this->fetch('content') ?>
        </div>
    </main>

    <?= $this->element('footer'); ?>

    <!-- js -->
    <?= $this->Html->script([
        'https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js',
        'base',
        'project',
    ]) ?>

    <?= $this->fetch('script') ?>
</body>
